﻿using Client.ApiTesters;
using Client.Repositories;
using Newtonsoft.Json;
using ProjectStructure.Common;
using System;
using System.Collections.Generic;

namespace Client
{
    class Program
    {
        static void Main(string[] args)
        {
            var repo = new ProjectsClientRepo();
            var pt = new ProjectsApiTester(repo);
            pt.GenerateProjects(10);
            //pt.SendProjects();

            IEnumerable<ProjectDTO> prs = pt.GetAllProjects().GetAwaiter().GetResult();
            var c = JsonConvert.SerializeObject(prs, Formatting.Indented);
            var project = pt.GetOneProject((prs as List<ProjectDTO>)[0].Id).GetAwaiter().GetResult();
            project.Name = "Paratrooper";
            pt.UpdateProject(project).GetAwaiter();
            pt.DeleteProject(2).GetAwaiter();
            IEnumerable<ProjectDTO> prsa = pt.GetAllProjects().GetAwaiter().GetResult();
            var ca = JsonConvert.SerializeObject(prs, Formatting.Indented);
            Console.WriteLine(c);
            Console.ReadLine();
        }
    }
}
