﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Client
{
    public class LinqTasksExemples
    {
        private LinqTaskExecutor te;

        public LinqTasksExemples(LinqTaskExecutor te)
        {
            this.te = te;
        }

        public void ShowExemples()
        {
            Console.WriteLine("------Task 1 --------");
            var x1 = te.T1_GetNumberTasksInProjectOfUser(39);
            foreach (var item in x1)
            {
                Console.WriteLine($"{item.Key} {item.Value}");
            }
            PressEnterToContinue();

            Console.WriteLine("------Task 2 --------");
            var x2 = te.T2_GetTasksForUser(2);
            foreach (var item in x2)
            {
                Console.WriteLine(item.Id);
            }
            PressEnterToContinue();

            Console.WriteLine("------Task 4 --------");
            var x4 = te.T4_GetTeamsWithSortedUsers();

            foreach (var item in x4)
            {
                Console.WriteLine($"{item.Item1} , {item.Item2} ");
                Console.WriteLine("\t{");
                foreach (var i in item.Item3)
                {
                    Console.WriteLine($"\t{i.RegisteredAt}");
                }
                Console.WriteLine("\t}");
            }
            PressEnterToContinue();

            Console.WriteLine("------Task 3--------");
            var x3 = te.T3_GetFinishedTasksForUser(15);
            foreach (var item in x3)
            {
                Console.WriteLine($"{item.Item1} , {item.Item2} ");

            }
            PressEnterToContinue();

            Console.WriteLine("------Task 5 --------");
            var x5 = te.T5_GetSortedUsersWithSortedTasks();
            foreach (var item in x5)
            {
                Console.WriteLine($"{item.Item1.FirstName} ");
                Console.WriteLine("");
                foreach (var i in item.Item2)
                {
                    Console.WriteLine($"\t{i.Name}");
                }
                Console.WriteLine("");
            }
            PressEnterToContinue();

            Console.WriteLine("------Task 6 --------");
            var x6 = te.T6_GetUserInfo(39);
            Console.WriteLine($"user {x6.user.Id}, lastProjId {x6.lastProject.Id}, tasksNumber {x6.lastProjectTasksNumber}," +
                $" notCompleted {x6.notCompletedTasksNumber}, longestTaskId {x6.longestTask.Id}");
            PressEnterToContinue();

            Console.WriteLine("------Task 7 --------");
            var x7 = te.T7_GetProjectInfo(22);
            Console.WriteLine($"proj {x7.project.Id}, longest {x7.longest.Id}, shortest {x7.shortest.Id}," +
                $" usersNumber {x7.usersNumber}");

            Console.ReadLine();
        }

        public static void PressEnterToContinue()
        {
            Console.WriteLine("Press enter to continue...");
            Console.ReadLine();
        }
    }
}
