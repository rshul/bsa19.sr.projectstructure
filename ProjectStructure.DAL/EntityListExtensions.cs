using ProjectStructure.Common;
using ProjectStructure.Common.Interfaces;
using System.Collections.Generic;

namespace ProjectStructure.DAL
{
    public static class EntityListExtensions
    {
        static int ProjectDTOidCount = 0;
        static int ProjectTaskDTOidCount = 0;
        static int UserDTOidCount = 0;
        static int TeamDTOidCount = 0;
        static int TaskStateDTOidCount = 0;

        public static int AutoIncAdd<T>(this List<T> ls, T entity) where T : class, IEntity
        {
            if (entity is ProjectDTO)
            {
                entity.Id = ProjectDTOidCount++;
            }
            else if (entity is ProjectTaskDTO)
            {
                entity.Id = ProjectTaskDTOidCount++;
            }
            else if (entity is UserDTO)
            {
                entity.Id = UserDTOidCount++;
            }
            else if (entity is TeamDTO)
            {
                entity.Id = TeamDTOidCount++;
            }
            else if (entity is TaskStateDTO)
            {
                entity.Id = TaskStateDTOidCount++;
            }
            else
            {
                throw new System.ArgumentException("Wrong type agrument");
            }
            ls.Add(entity);
            return entity.Id;
        }


    }
}