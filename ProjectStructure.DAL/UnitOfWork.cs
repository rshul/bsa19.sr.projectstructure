﻿using ProjectStructure.Common;
using ProjectStructure.DAL.Interfaces;
using ProjectStructure.DAL.Repositories;

namespace ProjectStructure.DAL
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ProjectsContext _context = new ProjectsContext();
        private IRepository<ProjectDTO> _projectRepository;

        private IRepository<UserDTO> _userRepository;

        private IRepository<TeamDTO> _teamRepository;

        private IRepository<TaskStateDTO> _taskStateRepository;

        private IRepository<ProjectTaskDTO> _projectTaskRepository;

        public IRepository<ProjectDTO> ProjectRepository => _projectRepository?? new GenericRepository<ProjectDTO>(_context.Projects);

        public IRepository<UserDTO> UserRepository => _userRepository?? new GenericRepository<UserDTO>(_context.Users);

        public IRepository<TeamDTO> TeamRepository => _teamRepository?? new GenericRepository<TeamDTO>(_context.Teams);

        public IRepository<TaskStateDTO> TaskStateRepository => _taskStateRepository?? new GenericRepository<TaskStateDTO>(_context.TaskStates);

        public IRepository<ProjectTaskDTO> ProjectTaskRepository => _projectTaskRepository?? new GenericRepository<ProjectTaskDTO>(_context.ProjectTasks);
    }
}
