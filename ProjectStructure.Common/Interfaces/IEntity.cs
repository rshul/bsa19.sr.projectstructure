﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectStructure.Common.Interfaces
{
    public interface IEntity
    {
        int Id { get; set; }
    }
}
