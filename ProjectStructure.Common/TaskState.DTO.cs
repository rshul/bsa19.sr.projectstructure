using ProjectStructure.Common.Interfaces;
namespace ProjectStructure.Common
{
    public class TaskStateDTO : IEntity
    {
        public int Id { get; set; }
        public string Value { get; set; }
    }
}