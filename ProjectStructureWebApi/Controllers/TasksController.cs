using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.Common;

namespace ProjectStructureWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        private IService<ProjectTaskDTO> _taskService;

        public TasksController(IService<ProjectTaskDTO> taskService)
        {
            _taskService = taskService;
        }

        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<ProjectTaskDTO>> Get()
        {
            return Ok(_taskService.GetAll());
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<ProjectTaskDTO> Get(int id)
        {
            ProjectTaskDTO foundEntity;
            try
            {
                foundEntity = _taskService.Get(id);
            }
            catch (DataEntityAccessException e)
            {
                return BadRequest(e.Message);
            }
            return Ok(foundEntity);
        }

        // POST api/values
        [HttpPost]
        public int Post([FromBody] ProjectTaskDTO value)
        {
            return _taskService.Create(value);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public ActionResult Put(int id, [FromBody] ProjectTaskDTO value)
        {
             try
            {
                if (id == value.Id)
                {
                    _taskService.Update(id, value);
                }
            }
            catch (DataEntityAccessException e)
            {
                return BadRequest(e.Message);
            }
            return NoContent();
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            try
            {
                _taskService.Delete(id);
            }
            catch (DataEntityAccessException e)
            {
                return BadRequest(e.Message);
            }
            return NoContent();
        }

    
    }
}