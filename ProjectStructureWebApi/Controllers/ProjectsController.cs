
using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.Common;


namespace ProjectStructureWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        private IService<ProjectDTO> _projectService;

        public ProjectsController(IService<ProjectDTO> projectService)
        {
            _projectService = projectService;
        }

        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<ProjectDTO>> Get()
        {
            return Ok(_projectService.GetAll());
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<ProjectDTO> Get(int id)
        {
            ProjectDTO foundEntity;
            try
            {
                foundEntity = _projectService.Get(id);
            }
            catch (DataEntityAccessException e)
            {
                return BadRequest(e.Message);
            }
            return Ok(foundEntity);
        }

        // POST api/values
        [HttpPost]
        public int Post([FromBody] ProjectDTO value)
        {
            return _projectService.Create(value);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public ActionResult Put(int id, [FromBody] ProjectDTO value)
        {
            try
            {
                if (id == value.Id)
                {
                    _projectService.Update(id, value);
                }
            }
            catch (DataEntityAccessException e)
            {
                return BadRequest(e.Message);
            }
            return NoContent();

        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            try
            {
                _projectService.Delete(id);
            }
            catch (DataEntityAccessException e)
            {
                return BadRequest(e.Message);
            }
            return NoContent();

        }

    }
}