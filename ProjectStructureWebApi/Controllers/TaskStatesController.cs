using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.Common;

namespace ProjectStructureWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TaskStatesController : ControllerBase
    {
        private IService<TaskStateDTO> _taskStateService;

        public TaskStatesController(IService<TaskStateDTO> taskStateService)
        {
            _taskStateService = taskStateService;
        }

        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<TaskStateDTO>> Get()
        {
            return Ok(_taskStateService.GetAll());
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<TaskStateDTO> Get(int id)
        {
            TaskStateDTO foundEntity;
            try
            {
                foundEntity = _taskStateService.Get(id);
            }
            catch (DataEntityAccessException e)
            {
                return BadRequest(e.Message);
            }
            return Ok(foundEntity);
        }

        // POST api/values
        [HttpPost]
        public int Post([FromBody] TaskStateDTO value)
        {
             return _taskStateService.Create(value);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public ActionResult Put(int id, [FromBody] TaskStateDTO value)
        {
             try
            {
                if (id == value.Id)
                {
                    _taskStateService.Update(id, value);
                }
            }
            catch (DataEntityAccessException e)
            {
                return BadRequest(e.Message);
            }
            return NoContent();
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            try
            {
                _taskStateService.Delete(id);
            }
            catch (DataEntityAccessException e)
            {
                return BadRequest(e.Message);
            }
            return NoContent();
        }






    }
}