using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.Common;

namespace ProjectStructureWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private IService<UserDTO> _userService;

        public UsersController(IService<UserDTO> userService)
        {
            _userService = userService;
        }

        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<UserDTO>> Get()
        {
            return Ok(_userService.GetAll());
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<UserDTO> Get(int id)
        {
           UserDTO foundEntity;
            try
            {
                foundEntity = _userService.Get(id);
            }
            catch (DataEntityAccessException e)
            {
                return BadRequest(e.Message);
            }
            return Ok(foundEntity);
        }

        // POST api/values
        [HttpPost]
        public int Post([FromBody] UserDTO value)
        {
            return _userService.Create(value);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public ActionResult Put(int id, [FromBody] UserDTO value)
        {
            try
            {
                if (id == value.Id)
                {
                    _userService.Update(id, value);
                }
            }
            catch (DataEntityAccessException e)
            {
                return BadRequest(e.Message);
            }
            return NoContent();
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            try
            {
                _userService.Delete(id);
            }
            catch (DataEntityAccessException e)
            {
                return BadRequest(e.Message);
            }
            return NoContent();
        }




    }
}